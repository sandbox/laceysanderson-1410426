<?php

/**
 * All GMOD RPC resultsets include the api version, data provider, etc. This function returns
 * an empty resultset containing only these data. Then each service will add it's results
 *
 * @return
 *   An empty resultset
 */
function tripal_gmod_rpc_v1_1_default_resultset () {

  $resultset = array(
    'api_version' => '1.1',
    'data_provider' => variable_get('tripal_gmodrpc_data_provider', NULL),
    'data_version' => variable_get('tripal_gmodrpc_feature_release', date('Y-m-d H:i:s')),
    'result' => array()  
  );
  
  if (empty($resultset['data_version'])) {
  	$resultset['data_version'] = date('Y-m-d H:i:s');
  }
  
  return $resultset;
  
}

/**
 * All Menu items ties to GMOD RPC v1.1
 *
 * @return 
 *   An array of menu items to be used in hook_menu
 */
function tripal_gmod_rpc_v1_1_menu() {
	$items = array();
	
	// Organism Listing
	$items['gmodrpc/v1.1/organisms'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Organism Listing'),
			'page callback' => 'tripal_gmod_rpc_v1_1_organism_list',
			'page arguments' => array('xml'),
			'access arguments' => array('access chado_organism content')
	);
	$items['gmodrpc/v1.1/organisms.xml'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Organism Listing -XML'),
			'page callback' => 'tripal_gmod_rpc_v1_1_organism_list',
			'page arguments' => array('xml'),
			'access arguments' => array('access chado_organism content')
	);
	$items['gmodrpc/v1.1/organisms.json'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Organism Listing -JSON'),
			'page callback' => 'tripal_gmod_rpc_v1_1_organism_list',
			'page arguments' => array('json'),
			'access arguments' => array('access chado_organism content')
	);

  // Full Text Feature Search
	$items['gmodrpc/v1.1/fulltext/%'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Full Text Search'),
			'page callback' => 'tripal_gmod_v1_1_gene_fulltext',
			'page arguments' => array('xml',3),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/fulltext/%.xml'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Full Text Search -XML'),
			'page callback' => 'tripal_gmod_v1_1_gene_fulltext',
			'page arguments' => array('xml', 3),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/fulltext/%.json'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Full Text Search -JSON'),
			'page callback' => 'tripal_gmod_v1_1_gene_fulltext',
			'page arguments' => array('json', 3),
			'access arguments' => array('access chado_feature content')
	);
	
	// Feature Location Search
	$items['gmodrpc/v1.1/location/chromosome/%'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Feature Location Search'),
			'page callback' => 'tripal_gmod_v1_1_feature_location',
			'page arguments' => array('xml',4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/location/chromosome/%.xml'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Feature Location Search -XML'),
			'page callback' => 'tripal_gmod_v1_1_feature_location',
			'page arguments' => array('xml', 4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/location/chromosome/%.json'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Feature Location Search -JSON'),
			'page callback' => 'tripal_gmod_v1_1_feature_location',
			'page arguments' => array('json', 4),
			'access arguments' => array('access chado_feature content')
	);	
	
	// Gene Ontology Search
	$items['gmodrpc/v1.1/ontology/gene/%'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Ontology Search'),
			'page callback' => 'tripal_gmod_v1_1_feature_by_ontology',
			'page arguments' => array('xml',4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/ontology/gene/%.xml'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Ontology Search -XML'),
			'page callback' => 'tripal_gmod_v1_1_feature_by_ontology',
			'page arguments' => array('xml', 4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/ontology/gene/%.json'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Ontology Search -JSON'),
			'page callback' => 'tripal_gmod_v1_1_feature_by_ontology',
			'page arguments' => array('json', 4),
			'access arguments' => array('access chado_feature content')
	);		

	// Gene Ortholog Search
	$items['gmodrpc/v1.1/orthologs/gene/%'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Orthology Search'),
			'page callback' => 'tripal_gmod_v1_1_feature_ortholog_search',
			'page arguments' => array('xml',4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/orthologs/gene/%.xml'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Orthology Search -XML'),
			'page callback' => 'tripal_gmod_v1_1_feature_ortholog_search',
			'page arguments' => array('xml', 4),
			'access arguments' => array('access chado_feature content')
	);
	$items['gmodrpc/v1.1/orthologs/gene/%.json'] = array(
			'type' => MENU_CALLBACK,
			'title' => t('Gene Orthology Search -JSON'),
			'page callback' => 'tripal_gmod_v1_1_feature_ortholog_search',
			'page arguments' => array('json', 4),
			'access arguments' => array('access chado_feature content')
	);	
	
	return $items;
}

/**
 * GMOD RPC (v1.1) Organism List
 *
 * Lists all organisms available through this site
 *
 * @param $return_type
 *   one of xml or json
 *
 * @return
 *   organism details for all organims in the format specified by $return_type
 */
function tripal_gmod_rpc_v1_1_organism_list ($return_type) {
	
	// Retrieve results from chado
	$chado_results = tripal_core_chado_select('organism',array('genus','species','common_name', 'abbreviation'), array());
	
	// Convert array into correct format
	$results = array(
		'resultset' => tripal_gmod_rpc_v1_1_default_resultset()
	);
	foreach ($chado_results as $r) {
		$results['resultset']['result'][]['organism'] = (array) $r;
	}
	
	// Output results in requested format
	switch ($return_type) {
		case 'xml':
			$xml_obj = new SimpleXMLElement("<resultset></resultset>");
			tripal_gmod_rpc_array_to_xml_object($results['resultset'], $xml_obj);
			print check_plain($xml_obj->asXML());
		break;
		case 'json':
			print json_encode($results);
		break;
	}

}

/**
 * GMOD RPC (v1.1) Gene Full Text 
 *
 * Provides a full text search for all features where by full text it's meant that the
 * search term appears on the node page.
 *
 * NOTE: Uses the Drupal search index. As such, only content indexed by Drupal 
 * will show up in these search results
 *
 * @param $return_type
 *   one of xml or json
 * @param $search_term
 *   the term to search for using fulltext search
 *
 * @query_param type
 *   the name or SO number of the type of features to return
 * @query_param genus
 *   the genus of the organism to restrict features to
 * @query_param species
 *   the species of the organism to restrict features to
 * @query_param taxid
 *   the taxonimy id  (organismprop with type=taxid) of the organism to restrict features to
 *
 * @return
 *   The dbxrefs of all features with the search term and matching the query paramaters
 *   in the format specified by $return_type
 */
function tripal_gmod_v1_1_gene_fulltext ($return_type, $search_term) {
  
  // Parameters -----------------------
  if (preg_match('/(.+)\.(xml|json)/',$search_term, $matches)) {
    $search_term = $matches[1];
    $return_type = $matches[2];
  }
  
  // get type_id
  if ($_GET['type']) {
    $feature_type = tripal_gmod_rpc_get_type_id($_GET['type']);
  }
  
  // get organism_id
  $organisms = tripal_gmod_rpc_get_organisms($_GET);
  
  // Raw Results (Chado) --------------
  $raw_results = tripal_gmod_rpc_do_search($search_term, 'chado_feature');
  
  // Process to RPC Results -----------
  $dbxref_sql = "SELECT db.name as db, dbxref.accession, db.urlprefix 
            FROM dbxref
            LEFT JOIN db db ON db.db_id=dbxref.db_id
            WHERE dbxref_id IN (SELECT dbxref_id FROM feature WHERE feature_id=%d)
              OR dbxref_id IN (SELECT dbxref_id FROM feature_dbxref WHERE feature_id=%d)";
  $feature_sql = "SELECT organism_id, type_id, feature_id, dbxref_id, timeaccessioned FROM feature WHERE feature_id=%d";
  $modified_sql = "SELECT changed FROM node WHERE nid=%d";
  
	$results = array(
		'resultset' => tripal_gmod_rpc_v1_1_default_resultset()
	);
  foreach ($raw_results as $r) {
    $item = array();
    
    // if result is a feature
    if ($r->chado_type == 'feature') {

      $previous_db = tripal_db_set_active('chado');
      $feature = db_fetch_object(db_query($feature_sql, $r->feature_id));
      tripal_db_set_active($previous_db);
      
      if (!empty($organisms)) {
      	if (!in_array($feature->organism_id, $organisms)) {
      		// Organism was provided in path
      		// And this feature is of a different organism
      		// Therefore, don't add this item
      		continue;
      	}
      }
      
      if (!empty($feature_type)) {
      	if ($feature_type != $feature->type_id) {
      		// A feature type was provided in the path
      		// And this feature is of a different type
      		// Therefore, don't add this item
      		continue;
      	}
      }
    
			//get dbxref associated with that feature
			$previous_db = tripal_db_set_active('chado');
			$dbxref_resource = db_query($dbxref_sql, $r->feature_id, $r->feature_id);
			tripal_db_set_active($previous_db);
			while ($dbxref = db_fetch_array($dbxref_resource)) {
				if (!$dbxref['urlprefix']) {
					unset($dbxref['urlprefix']);
				} else {
					$dbxref['url'] = $dbxref['urlprefix'] . $dbxref['accession'];
					unset($dbxref['urlprefix']);
				}
				$item['dbxref'][] = $dbxref;
			}
			
			// get the date created (chado) 
			$item['date_created'] = $feature->timeaccessioned;
			
			// and last modified (drupal)
			$modified = db_fetch_object(db_query($modified_sql, $r->nid));
			$item['last_modified'] = date('Y-m-d H:i:s', $modified->changed);
      
      // Only add to results if there is at least one dbxref
      if (isset($item['dbxref'])) {
        $results['resultset']['result'][] = $item;
      }
    }
  }
  
	// Output results -------------------
	switch ($return_type) {
		case 'xml':
			$xml_obj = new SimpleXMLElement("<resultset></resultset>");
			tripal_gmod_rpc_array_to_xml_object($results['resultset'], $xml_obj);
			print check_plain($xml_obj->asXML());
		break;
		case 'json':
			print json_encode($results);
		break;
	}
	
}

/**
 * GMOD RPC (v1.1) Gene Location Search
 *
 * Fetches all features located on a specified parent feature.
 *
 * @param $return_type
 *   one of xml or json
 * @param $srcfeature_name
 *   The name of the parent feature
 *
 * @query_param type
 *   the name or SO number of the type of features to return
 * @query_param genus
 *   the genus of the organism to restrict features to
 * @query_param species
 *   the species of the organism to restrict features to
 * @query_param taxid
 *   the taxonimy id  (organismprop with type=taxid) of the organism to restrict features to
 * @query_param fmin
 *   find all features that start on or after this position on the srcfeature
 * @query_param fmax
 *   find all features that end on or before this position on the srcfeature
 * @query_param strand
 *   find all features located on this strand of the srcfeature (1 or -1)
 *
 * @return
 *   The dbxrefs of all features with the search term and matching the query paramaters
 *   in the format specified by $return_type
 */
function tripal_gmod_v1_1_feature_location ($return_type, $srcfeature_name) {

  // Parameters -----------------------
  if (preg_match('/(.+)\.(xml|json)/',$srcfeature_name, $matches)) {
    $srcfeature_name = $matches[1];
    $return_type = $matches[2];
  }
  
  // Query Arguments Prep -------------
  // Type
  $where_str = array();
  $where_arg = array();
  if ($_GET['type']) {
    $feature_type = tripal_gmod_rpc_get_type_id($_GET['type']);
    if ($feature_type) {
      $where_str[] = "f.type_id=%d";
      $where_arg[] = $feature_type;
    }
  }
  
  // Featureloc
  if (isset($_GET['fmin']) && is_numeric($_GET['fmin'])) {
    $where_str[] = 'fl.fmin >= %d';
    $where_arg[] = (int) $_GET['fmin'];
  }
  if (isset($_GET['fmax']) && is_numeric($_GET['fmax'])) {
    $where_str[] = 'fl.fmax <= %d';
    $where_arg[] = (int) $_GET['fmax'];
  }
  if (isset($_GET['strand']) && ($_GET['strand'] == '-1' OR $_GET['strand'] == '1')) {
    $where_str[] = "fl.strand='%s'";
    $where_arg[] = $_GET['strand'];
  }
  
  // Organism
  $organisms = tripal_gmod_rpc_get_organisms($_GET);
  if (sizeof($organisms) == 1) {
    $where_str[] = "f.organism_id=%d";
    $where_arg[] = $organisms[0];
  } elseif (sizeof($organisms) > 1) {
    $where_str[] = "f.organism_id IN (".str_repeat('%d,',sizeof($organisms)-1)."%d)";
    $where_arg = array_merge($where_arg, $organisms);
  }
  
  // Get Results ----------------------
  $dbxref_sql = "SELECT db.name as db, dbxref.accession, db.urlprefix 
            FROM dbxref
            LEFT JOIN db db ON db.db_id=dbxref.db_id
            WHERE dbxref_id IN (SELECT dbxref_id FROM feature WHERE feature_id=%d)
              OR dbxref_id IN (SELECT dbxref_id FROM feature_dbxref WHERE feature_id=%d)";
  $modified_sql = "SELECT changed FROM {node} WHERE nid IN (SELECT nid FROM {chado_feature} WHERE feature_id=%d)";
  
  $results = array(
		'resultset' => tripal_gmod_rpc_v1_1_default_resultset()
	);  
	
  $sql = "SELECT f.organism_id, f.type_id, f.feature_id, f.dbxref_id, f.timeaccessioned, f.timelastmodified FROM feature f ";
  $joins[] = "LEFT JOIN featureloc fl ON fl.feature_id=f.feature_id";
  $where_str[] = "fl.srcfeature_id IN (SELECT feature_id FROM feature WHERE name='%s' OR uniquename='%s')";
  $where_arg[] = $srcfeature_name;
  $where_arg[] = $srcfeature_name;
  
  $sql .= implode(" ",$joins);
  $sql .= ' WHERE '.implode(' AND ',$where_str);

  $previous_db = tripal_db_set_active('chado');
  $resource = db_query($sql, $where_arg);
  tripal_db_set_active($previous_db);  

  
  while ($r = db_fetch_object($resource)) {
    $item = array();
    
    //get dbxref associated with that feature
    $previous_db = tripal_db_set_active('chado');
    $dbxref_resource = db_query($dbxref_sql, $r->feature_id, $r->feature_id);
    tripal_db_set_active($previous_db);
    while ($dbxref = db_fetch_array($dbxref_resource)) {
      if (!$dbxref['urlprefix']) {
        unset($dbxref['urlprefix']);
      } else {
        $dbxref['url'] = $dbxref['urlprefix'] . $dbxref['accession'];
        unset($dbxref['urlprefix']);
      }
      $item['dbxref'][] = $dbxref;
    }
    
    // get the date created (chado) 
    $item['date_created'] = $r->timeaccessioned;
    
    // and last modified (drupal)
    $modified = db_fetch_object(db_query($modified_sql, $r->feature_id));
    $item['last_modified'] = date('Y-m-d H:i:s', $modified->changed);
    if (empty($item['last_modified'])) {
      $item['last_modified'] = $r->timelastmodified;
    }
    
    // Only add to results if there is at least one dbxref
    if (isset($item['dbxref'])) {
      $results['resultset']['result'][] = $item;
    }
  }

	// Output results -------------------
	switch ($return_type) {
		case 'xml':
			$xml_obj = new SimpleXMLElement("<resultset></resultset>");
			tripal_gmod_rpc_array_to_xml_object($results['resultset'], $xml_obj);
			print check_plain($xml_obj->asXML());
		break;
		case 'json':
			print json_encode($results);
		break;
	}
	
}

/**
 * GMOD RPC (v1.1) Gene Ontology Search
 *
 * Search for genes that have been annotated with a particular gene ontology ID
 *
 * @param $return_type
 *   one of xml or json
 * @param $ontology_term
 *   find all features annotated with this ontology term
 *
 * @query_param genus
 *   the genus of the organism to restrict features to
 * @query_param species
 *   the species of the organism to restrict features to
 * @query_param taxid
 *   the taxonimy id  (organismprop with type=taxid) of the organism to restrict features to
 *
 * @return
 *   The dbxrefs of all features with the search term and matching the query paramaters
 *   in the format specified by $return_type
 */
function tripal_gmod_v1_1_feature_by_ontology ($return_type, $ontology_term) {

  // Parameters -----------------------
  if (preg_match('/(.+)\.(xml|json)/',$srcfeature_name, $matches)) {
    $ontology_term = $matches[1];
    $return_type = $matches[2];
  }
  
  // Query Arguments Prep -------------
  $where_str = '';
  $where_arg = array();
  
  // Organism
  $organisms = tripal_gmod_rpc_get_organisms($_GET);
  if (sizeof($organisms) == 1) {
    $where_str[] = "f.organism_id=%d";
    $where_arg[] = $organisms[0];
  } elseif (sizeof($organisms) > 1) {
    $where_str[] = "f.organism_id IN (".str_repeat('%d,',sizeof($organisms)-1)."%d)";
    $where_arg = array_merge($where_arg, $organisms);
  }

  // Get Results ----------------------
  $results = array(
		'resultset' => tripal_gmod_rpc_v1_1_default_resultset()
	);   
	
	$term_id = tripal_gmod_rpc_get_type_id($ontology_term);
	
	if ($term_id) {
	  $dbxref_sql = "SELECT db.name as db, dbxref.accession, db.urlprefix 
            FROM dbxref
            LEFT JOIN db db ON db.db_id=dbxref.db_id
            WHERE dbxref_id IN (SELECT dbxref_id FROM feature WHERE feature_id=%d)
              OR dbxref_id IN (SELECT dbxref_id FROM feature_dbxref WHERE feature_id=%d)";
    $modified_sql = "SELECT changed FROM {node} WHERE nid IN (SELECT nid FROM {chado_feature} WHERE feature_id=%d)";
  
	  $sql = "SELECT f.organism_id, f.type_id, f.feature_id, f.dbxref_id, f.timeaccessioned, f.timelastmodified FROM feature f ";
	  $joins[] = "LEFT JOIN feature_cvterm fc ON fc.feature_id=f.feature_id";
	  
	  if (is_array($term_id)) {
  	  $where_str[] = "fc.cvterm_id IN (".str_repeat('%d,',sizeof($term_id)-1)."%d)";
	    $where_arg = array_merge($where_arg, $term_id);
	  } else {
  	  $where_str[] = "fc.cvterm_id=%d";
	    $where_arg[] = $term_id;
	  }
	  
    $sql .= implode(" ",$joins);
    $sql .= ' WHERE '.implode(' AND ',$where_str);
    
    $previous_db = tripal_db_set_active('chado');
    $resource = db_query($sql, $where_arg);
    tripal_db_set_active($previous_db);  
  
    
    while ($r = db_fetch_object($resource)) {
      $item = array();
      
      //get dbxref associated with that feature
      $previous_db = tripal_db_set_active('chado');
      $dbxref_resource = db_query($dbxref_sql, $r->feature_id, $r->feature_id);
      tripal_db_set_active($previous_db);
      while ($dbxref = db_fetch_array($dbxref_resource)) {
        if (!$dbxref['urlprefix']) {
          unset($dbxref['urlprefix']);
        } else {
          $dbxref['url'] = $dbxref['urlprefix'] . $dbxref['accession'];
          unset($dbxref['urlprefix']);
        }
        $item['dbxref'][] = $dbxref;
      }
      
      // get the date created (chado) 
      $item['date_created'] = $r->timeaccessioned;
      
      // and last modified (drupal)
      $modified = db_fetch_object(db_query($modified_sql, $r->feature_id));
      $item['last_modified'] = date('Y-m-d H:i:s', $modified->changed);
      if (empty($item['last_modified'])) {
        $item['last_modified'] = $r->timelastmodified;
      }
      
      // Only add to results if there is at least one dbxref
      if (isset($item['dbxref'])) {
        $results['resultset']['result'][] = $item;
      }
    }
	  
	}
  
	// Output results -------------------
	switch ($return_type) {
		case 'xml':
			$xml_obj = new SimpleXMLElement("<resultset></resultset>");
			tripal_gmod_rpc_array_to_xml_object($results['resultset'], $xml_obj);
			print check_plain($xml_obj->asXML());
		break;
		case 'json':
			print json_encode($results);
		break;
	}
	
}

/**
 * GMOD RPC (v1.1) Gene Ortholog Search
 *
 * Returns all features orthologous to the indicated gene
 *
 * NOTE: For two features to be considered orthologous, they should be linked through 
 *       feature_relationship by a relationship where the type is orthologus_to (Sequence Ontology)
 *
 * @param $return_type
 *   one of xml or json
 * @param feature_identifier
 *   find all features orthologous to this feature. This could be feature.name, feature.uniquename 
 *   or dbxref.accession linked through either feature.dbxref_id or feature_dbxref
 *
 * @query_param genus
 *   the genus of the organism to restrict features to
 * @query_param species
 *   the species of the organism to restrict features to
 * @query_param taxid
 *   the taxonimy id  (organismprop with type=taxid) of the organism to restrict features to
 *
 * @return
 *   The dbxrefs of all features orthologous to the given feature and matching the query paramaters
 *   in the format specified by $return_type
 */
function tripal_gmod_v1_1_feature_ortholog_search ($return_type, $feature_identifier) {


  // Parameters -----------------------
  if (preg_match('/(.+)\.(xml|json)/',$srcfeature_name, $matches)) {
    $ontology_term = $matches[1];
    $return_type = $matches[2];
  }
  
  // Query Arguments Prep -------------
  $where_str = '';
  $where_arg = array();
  
  // Organism
  $organisms = tripal_gmod_rpc_get_organisms($_GET);
  if (sizeof($organisms) == 1) {
    $where_str[] = "f.organism_id=%d";
    $where_arg[] = $organisms[0];
  } elseif (sizeof($organisms) > 1) {
    $where_str[] = "f.organism_id IN (".str_repeat('%d,',sizeof($organisms)-1)."%d)";
    $where_arg = array_merge($where_arg, $organisms);
  }

  // Get Results ----------------------
  $results = array(
		'resultset' => tripal_gmod_rpc_v1_1_default_resultset()
	);   
	
	$feature_id = tripal_gmod_rpc_get_feature_by_identifier ($feature_identifier);
	
	if ($feature_id) {
    $term_id = tripal_gmod_rpc_get_type_id('orthologous_to');
    
    if ($term_id) {
      $dbxref_sql = "SELECT db.name as db, dbxref.accession, db.urlprefix 
              FROM dbxref
              LEFT JOIN db db ON db.db_id=dbxref.db_id
              WHERE dbxref_id IN (SELECT dbxref_id FROM feature WHERE feature_id=%d)
                OR dbxref_id IN (SELECT dbxref_id FROM feature_dbxref WHERE feature_id=%d)";
      $modified_sql = "SELECT changed FROM {node} WHERE nid IN (SELECT nid FROM {chado_feature} WHERE feature_id=%d)";
    
      $sql = "SELECT f.organism_id, f.type_id, f.feature_id, f.dbxref_id, f.timeaccessioned, f.timelastmodified FROM feature f ";
      $joins[] = "LEFT JOIN feature_relationship fr_sub ON fr_sub.subject_id=f.feature_id";
      $joins[] = "LEFT JOIN feature_relationship fr_obj ON fr_obj.object_id=f.feature_id";
      
      // Add in type restriction
      $where_str[] = "(fr_sub.type_id=%d OR fr_obj.type_id=%d)";
      $where_arg[] = $term_id;
      $where_arg[] = $term_id;
      
      // Restrict to feature
      $where_str[] = "(fr_sub.object_id=%d OR fr_obj.subject_id=%d)";
      $where_arg[] = $feature_id;   
      $where_arg[] = $feature_id;  
      
      $sql .= implode(" ",$joins);
      $sql .= ' WHERE '.implode(' AND ',$where_str);
      
      $previous_db = tripal_db_set_active('chado');
      $resource = db_query($sql, $where_arg);
      tripal_db_set_active($previous_db);  
    
      
      while ($r = db_fetch_object($resource)) {
        $item = array();
        
        //get dbxref associated with that feature
        $previous_db = tripal_db_set_active('chado');
        $dbxref_resource = db_query($dbxref_sql, $r->feature_id, $r->feature_id);
        tripal_db_set_active($previous_db);
        while ($dbxref = db_fetch_array($dbxref_resource)) {
          if (!$dbxref['urlprefix']) {
            unset($dbxref['urlprefix']);
          } else {
            $dbxref['url'] = $dbxref['urlprefix'] . $dbxref['accession'];
            unset($dbxref['urlprefix']);
          }
          $item['dbxref'][] = $dbxref;
        }
        
        // get the date created (chado) 
        $item['date_created'] = $r->timeaccessioned;
        
        // and last modified (drupal)
        $modified = db_fetch_object(db_query($modified_sql, $r->feature_id));
        $item['last_modified'] = date('Y-m-d H:i:s', $modified->changed);
        if (empty($item['last_modified'])) {
          $item['last_modified'] = $r->timelastmodified;
        }
        
        // Only add to results if there is at least one dbxref
        if (isset($item['dbxref'])) {
          $results['resultset']['result'][] = $item;
        }
      }
      
    }
  }
  
	// Output results -------------------
	switch ($return_type) {
		case 'xml':
			$xml_obj = new SimpleXMLElement("<resultset></resultset>");
			tripal_gmod_rpc_array_to_xml_object($results['resultset'], $xml_obj);
			print check_plain($xml_obj->asXML());
		break;
		case 'json':
			print json_encode($results);
		break;
	}
	
}