<?php

function tripal_gmod_rpc_administrative_form (&$form_state = array()) {
  $form = array();
  
  $form['generic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Information'),
    '#description' => t('The following fields are used to identify your Tripal site. This is included in
      each retrieved dataset to ensure you are given credit for your data.')
  );
  
  $form['generic']['data_provider'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#description' => t('This will be shown as the data_provider for all datasets originating from this Tripal website.'),
    '#default_value' => variable_get('tripal_gmodrpc_data_provider', NULL),
  );
  
  $form['feature'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sequence Feature Data'),
    '#description' => t('The following configuration options control various aspects of retrieveal/display of sequence feature data.'),
  );
  
  $form['feature']['feature_release'] = array(
    '#type' => 'textfield',
    '#title' => t('Current Data Release'),
    '#description' => t('If your website releases data in versioned batches. This is the 
      name/version of the current batch of features. If sequence feature data is updated 
      asynchronously then leave this field blank.'),
    '#default_value' => variable_get('tripal_gmodrpc_feature_release', NULL)
  );
  
  $cvs = array();
  $results = tripal_core_chado_select('cv',array('cv_id','name'),array());
  foreach ($results as $r) {
    $cvs[ $r->name ] = $r->name;
  }
  $key = array_search('sequence',$cvs);
  $form['feature']['sequence_cv']  = array(
    '#type' => 'select',
    '#title' => t('Sequence Ontology'),
    '#description' => t('Please select the Sequence Ontology from the following list.'),
    '#options' => $cvs,
    '#default_value' => variable_get('tripal_gmodrpc_sequence_cv', $key)
  );
  
  $dbs = array();
  $results = tripal_core_chado_select('db',array('db_id','name'),array());
  foreach ($results as $r) {
    $dbs[ $r->name ] = $r->name;
  }
  $key = array_search('SO',$dbs);
  $form['feature']['sequence_db']  = array(
    '#type' => 'select',
    '#title' => t('Sequence Ontology Term Database'),
    '#description' => t('Please select the database containing references to the sequence ontology terms.'),
    '#options' => $dbs,
    '#default_value' => variable_get('tripal_gmodrpc_sequence_db', $key)
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration')
  );
  
  return $form;
}

function tripal_gmod_rpc_administrative_form_submit ($form, &$form_state) {
	
	variable_set('tripal_gmodrpc_data_provider', $form_state['values']['data_provider']);
	
	variable_set('tripal_gmodrpc_feature_release',$form_state['values']['feature_release']);
	variable_set('tripal_gmodrpc_sequence_cv',$form_state['values']['sequence_cv']);
	variable_set('tripal_gmodrpc_sequence_db',$form_state['values']['sequence_db']);
}