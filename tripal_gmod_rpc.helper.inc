<?php

/**
 * Retrieves the cvterm_id based on name or dbxref accessions
 *
 * @param $type_name
 *   The name of a cvterm (either cvtern.name or cvterm.dbxref => dbxref.accession)
 * 
 * @return
 *   The cvterm_id
 */
function tripal_gmod_rpc_get_type_id ( $type_name ) {

    $resource = db_query(
    	"SELECT cvterm_id FROM cvterm WHERE name='%s'", 
    	$type_name
    );
    $terms = array();
    while ($r = db_fetch_object($resource)) {
      $terms[] = $r->cvterm_id;
    }
    
    if (preg_match('/(\w+):(\d+)/',$type_name, $matches)) {
      
      // Get cvterms where above is dbxref attached via cvterm.dbxref_id
      $sql = "SELECT cvterm_id 
              FROM cvterm 
              LEFT JOIN dbxref dbxref ON dbxref.dbxref_id=cvterm.dbxref_id
              WHERE accession='%s' 
                AND db_id IN (SELECT db_id FROM db WHERE name='%s')";
      $resource = db_query($sql, $matches[2], $matches[1]);
      while ($r = db_fetch_object($resource)) {
        $terms[] = $r->cvterm_id;
      }
      
      // Get cvterms where above is dbxref attached through cvterm_dbxref
      $sql = "SELECT cd.cvterm_id 
              FROM cvterm_dbxref cd
              LEFT JOIN dbxref dbx ON dbx.dbxref_id=cd.dbxref_id
              WHERE dbx.accession='%s' 
                AND dbx.db_id IN (SELECT db_id FROM db WHERE name='%s')";
      $resource = db_query($sql, $matches[2], $matches[1]);
      while ($r = db_fetch_object($resource)) {
        $terms[] = $r->cvterm_id;
      }
      
    }
    
    if (sizeof($terms) == 1) {
      return $terms[0];
    } elseif (sizeof($terms) == 0) {
      return FALSE;
    } else {
      return $terms;
    }
}

/**
 * Retrieves all organisms matching the given criteria
 *
 * @param $url_params
 *   usually $_GET array.
 *    - $url_params['genus'] = organism.genus
 *    - $url_params['species'] = organism.species
 *    - $url_params['taxid'] = organismprop.value
 *
 * @return
 *   An array of organisms matching the url parameters
 */
function tripal_gmod_rpc_get_organisms ($url_params) {

  $str = array();
  $args = array();
  if ($url_params['genus']) {
    $str[] = "organism.genus='%s'";
    $args[] = $url_params['genus'];
  }
  if ($url_params['species']) {
    $str[] = "organism.species='%s'";
    $args[] = $url_params['species'];
  }  
  if ($url_params['taxid']) {
    $str[] = "organismprop.value='%s'";
    $args[] = $url_params['taxid'];
  }
  if ($str) {
    $previous_db = tripal_db_set_active('chado');
    $sql = "SELECT distinct(organism.organism_id) FROM organism LEFT JOIN organismprop organismprop ON organismprop.organism_id=organism.organism_id WHERE ".implode(' AND ',$str);
    tripal_db_set_active($previous_db);
    $resource = db_query($sql,$args);
    while($r = db_fetch_object($resource)) {
      $organisms[] = $r->organism_id;
    }
  }
  
  return $organisms;
}

/**
 * Retrieve feature_id based on feature_identifier
 *
 * @param feature_identifier
 *   find all features orthologous to this feature. This could be feature.name, feature.uniquename 
 *   or dbxref.accession linked through either feature.dbxref_id or feature_dbxref
 *
 * @return
 *   feature_id of feature matching given identifier
 */
function tripal_gmod_rpc_get_feature_by_identifier ($feature_identifier) {
  $feature_id = array();
  
  // Retrieve any features with identifier as feature.name or feature.uniquename
  $sql = "SELECT feature_id FROM feature WHERE name='%s' OR uniquename='%s'";
  $previous_db = tripal_db_set_active('chado');
  $resource = db_query($sql, $feature_identifier, $feature_identifier);
  tripal_db_set_active($previous_db);
  while ($r = db_fetch_object($resource)) {
    $feature_id[] = $r->feature_id;
  }
  
  // Retrieve any features with identifier as accession (feature.dbxref_id)
  $sql = "SELECT f.feature_id FROM feature f 
    LEFT JOIN dbxref dbx ON dbx.dbxref_id=f.dbxref_id 
    WHERE dbx.accession='%s'";
  $previous_db = tripal_db_set_active('chado');
  $resource = db_query($sql, $feature_identifier);
  tripal_db_set_active($previous_db);
  while ($r = db_fetch_object($resource)) {
    $feature_id[] = $r->feature_id;
  }
  
  // Retrieve any features with identifier as accession (feature_dvxref.dbxref_id)
  $sql = "SELECT fd.feature_id FROM feature_dbxref fd
    LEFT JOIN dbxref dbx ON dbx.dbxref_id=fd.dbxref_id 
    WHERE dbx.accession='%s'";
  $previous_db = tripal_db_set_active('chado');
  $resource = db_query($sql, $feature_identifier);
  tripal_db_set_active($previous_db);
  while ($r = db_fetch_object($resource)) {
    $feature_id[] = $r->feature_id;
  }
  
  
  if (sizeof($feature_id) == 0) {
    return FALSE;
  } elseif (sizeof($feature_id) == 1) {
    return $feature_id[0];
  } else {
    return $feature_id;
  }
}